package main

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/extemporalgenome/slug"
	"github.com/flosch/pongo2/v6"
)

func main() {
	c := ReadConfig()
	var tpl = pongo2.Must(pongo2.FromFile("entry.tmpl"))
	// for pongo template context
	pctx := make(map[string]interface{})
	pctx["Author"] = c.SiteAuthor
	pctx["SiteName"] = c.SiteName
	pctx["Domain"] = c.SiteDomain
	// fmt.Printf("%+v\n", c)
	// for _, n := range c.Nav {
	// 	for k, v := range n {
	// 		fmt.Println(k, v)
	// 	}
	// }
	os.RemoveAll(c.Target)
	os.MkdirAll(c.Target, 0777)
	// copy 404 file
	nffile, _ := os.ReadFile("pgs/404.html")
	os.WriteFile(filepath.Join(c.Target, "404.html"), nffile, 0644)

	filepath.WalkDir(c.Source, func(fpath string, d fs.DirEntry, err error) error {
		fmt.Println("dealing with: ", fpath)

		// ignore . files like .DS
		// we are starting to read from /docs directory
		// so the fpath will contain something like /docs/.obsidian
		// we need to remove the source (/docs) to compare
		// fpath = original file path
		// fp = original filepath - source directory name
		fp := strings.Replace(fpath, c.Source, "", 1)
		if strings.HasPrefix(fp, "/.") {
			return nil
		}

		destDir, fname := filepath.Split(fp)
		destDir = slugifyDir(path.Join(c.Target, destDir))

		// if directory, create same in target
		// if it is not the source of course
		if d.IsDir() {
			if fpath == c.Source {
				return nil
			}

			os.MkdirAll(path.Join(destDir, slug.Slug(d.Name())), 0777)
			return nil
		}

		content, e := os.ReadFile(fpath)
		if e != nil {
			fmt.Println("Error reading file: ", e)
			return nil
		}

		// if markdown, process; else copy as is
		if filepath.Ext(fpath) == ".md" {
			mc := mdify(content)

			pctx["Content"] = mc
			tc, err := tpl.Execute(pctx)
			if err != nil {
				log.Fatal("Error applying template for: " + fpath + " is: " + err.Error())
			}
			content = []byte(tc)
			// replace .md to .html
			// then make it slug
			// slugify only .md file
			// that is why it is in this loop
			fname = slug.Slug(strings.TrimSuffix(fname, path.Ext(fname))) + ".html"
		}

		// at this point, if it is md file, it is slugified with .html ext
		// other files retain their filename
		fname = path.Join(destDir, fname)
		e = os.WriteFile(fname, content, 0644)
		if e != nil {
			fmt.Println("Error writing file: ", e)
			return nil
		}
		return nil
	})

}
