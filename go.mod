module gopub

go 1.19

require (
	github.com/extemporalgenome/slug v0.0.0-20150414033109-0320c85e32e0
	github.com/flosch/pongo2/v6 v6.0.0
	github.com/yuin/goldmark v1.5.3
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/kr/pretty v0.3.0 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	golang.org/x/text v0.5.0 // indirect
)
