package wikilink

import (
	"fmt"
	"strings"

	"github.com/extemporalgenome/slug"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
)

const (
	_open  byte = '['
	_pipe  byte = '|'
	_hash  byte = '#'
	_close byte = ']'
)

// KindWikiLink is the kind of the wikilink AST node.
var KindWikiLink = ast.NewNodeKind("WikiLink")

// Wikilink in its simple form looks like this: [[Note Title]]
// But it can get complicated
// [[Note Title|title]] or [[Note Title#section]] or [[Note Title#section|title]]
// or even [[#section]]
//
// Which means, a wikilink can contain
//
// Target, Section, Label
//
// all the three could be empty in some cases;
// if all three are empty, then it is an empty wikilink
//
// Target = page to which this link points;
// Label = text to be displayed for this link
// if | is there then whatever is after |
// else the title itself is label
// Section = section within a page to link to
// [[#section]] means link to a section within the current page
//
// in a folder of notes, it is possible to the target doesn't exist

type wlink struct {
	ast.BaseInline
	Raw     string
	Target  string
	Section string
	Label   string
	Exists  bool
}

// Kind reports the kind of this node.
func (n *wlink) Kind() ast.NodeKind {
	return KindWikiLink
}

// Dump dumps the Node to stdout.
func (n *wlink) Dump(src []byte, level int) {
	ast.DumpHelper(n, src, level, map[string]string{
		"Target": n.Target,
	}, nil)
}

type wlParser struct{}
type wlRenderer struct{}
type wlExtension struct{}

func (r *wlRenderer) RegisterFuncs(register renderer.NodeRendererFuncRegisterer) {
	register.Register(KindWikiLink, r.render)
}

func NewRenderer() renderer.NodeRenderer {
	return &wlRenderer{}
}

// WikiLink is a goldmark extension configured with default options.
var WikiLink = newWithDefaultOptions()

// New returns a goldmark extension that can be configured with options.
func New() goldmark.Extender {
	ext := newWithDefaultOptions()

	return ext
}

func newWithDefaultOptions() *wlExtension {
	return &wlExtension{}
}

func (wp *wlParser) Trigger() []byte {
	return []byte{'['}
}

func (ext *wlExtension) Extend(m goldmark.Markdown) {
	// The link parser is at priority 200 in goldmark so we need to be
	// lower than that to ensure that the "[" trigger fires.
	m.Parser().AddOptions(
		parser.WithInlineParsers(
			util.Prioritized(&wlParser{}, 199),
		),
	)
	m.Renderer().AddOptions(renderer.WithNodeRenderers(
		util.Prioritized(NewRenderer(), 500),
	))
}

// Parse implements the parser.Parser interface for Wikilinks in markdown
func (p *wlParser) Parse(parent ast.Node, block text.Reader, pc parser.Context) ast.Node {

	line, _ := block.PeekLine()
	// parse gets triggered with [
	// confirm it has 2nd [
	// also it has to be > 4 chars
	if len(line) <= 4 || line[1] != _open {
		return nil
	}

	openPos := 2
	closePos := -1
	// loop through the line to find `]]`
	for i := openPos; i < len(line)-1; i++ {
		if line[i] == _close && line[i+1] == _close {
			closePos = i
			break
		}
	}

	// Ignore empty wiki links.
	if openPos == closePos || closePos == -1 {
		return nil
	}

	// this will be like wiki link#sec|label
	// all of these could be empty
	// but not all of them at the same time
	raw := string(line[openPos:closePos])
	wl := &wlink{}
	wl.Raw = raw // will be used as is if file doesn't exists
	// start with assumption it is simple [[wiki link]]
	wl.Target = raw
	// is there label?
	bfr, aftr, isfnd := strings.Cut(raw, "|")
	if isfnd {
		wl.Label = aftr
		wl.Target = bfr
	}

	bfr, aftr, isfnd = strings.Cut(wl.Target, "#")
	if isfnd {
		wl.Target = bfr
		// if target#content then display as is
		if wl.Target != "" && wl.Label == "" {
			wl.Label = raw
		}
		// if #content then display only content
		if wl.Label == "" {
			wl.Label = aftr
		}
		wl.Section = aftr
	}

	// if there was no | then
	if wl.Label == "" {
		wl.Label = wl.Target
	}

	// possible the label is ../sources/podcast;
	// we want to display only the last part
	slbl := strings.Split(wl.Label, "/")
	wl.Label = slbl[len(slbl)-1]

	// target & section can't be nil
	if len(wl.Target) == 0 && len(wl.Section) == 0 {
		return nil
	}

	wl.Exists = true // TODO

	block.Advance((closePos + 2)) // 2 coz open delimitors = `[[`

	return wl
}

func (r *wlRenderer) render(w util.BufWriter, source []byte, node ast.Node, entering bool) (ast.WalkStatus, error) {
	if !entering {
		return ast.WalkContinue, nil
	}

	wl := node.(*wlink)
	out := ""
	// target might have / - ex: sub-dir/subdir/file.md
	s := strings.SplitN(wl.Target, "/", -1)
	ds := ""
	for i, v := range s {
		// if it is going to subdir then don't sluggify
		if v == ".." {
			ds = ds + v
		} else {
			ds = ds + slug.Slug(v)
		}
		if i != len(s)-1 {
			ds = ds + "/"
		}
	}

	// if it is linking internally [[#section]] then ds will be ""
	if ds != "" {
		wl.Target = ds + ".html"
	}

	if wl.Exists {
		if wl.Section != "" {
			out = fmt.Sprintf(`<a href="%s#%s">%s</a>`, wl.Target, slug.Slug(wl.Section), wl.Label)
		} else {
			out = fmt.Sprintf(`<a href="%s">%s</a>`, wl.Target, wl.Label)
		}
	} else {
		// file doesn't exists so return as is
		out = wl.Raw
	}

	w.Write([]byte(out))
	return ast.WalkContinue, nil
}
