package main

import (
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	SiteName        string              `yaml:"site_name"`
	SiteDescription string              `yaml:"site_description"`
	SiteAuthor      string              `yaml:"site_author"`
	SiteDomain      string              `yaml:"site_domain"`
	Source          string              `yaml:"source"`
	Assets          string              `yaml:"assets"`
	Target          string              `yaml:"target"`
	Nav             []map[string]string `yaml:"nav"`
}

func ReadConfig() Config {
	// Load the file; returns []byte
	f, err := os.ReadFile("gopub.yml")
	if err != nil {
		log.Fatal(err)
	}
	// Create an empty Car to be are target of unmarshalling
	var c Config

	// Unmarshal our input YAML file into empty Car (var c)
	if err := yaml.Unmarshal(f, &c); err != nil {
		log.Fatal(err)
	}

	return c
}
