package wikiimglink

import (
	"fmt"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
)

// parses for ![[imagelink.png]] and adds a img link

const (
	_open  string = "![["
	_close string = "]]"
)

// KindWikiImgLink is the kind of the wikilink AST node.
var KindWikiImgLink = ast.NewNodeKind("WikiImgLink")

type wimglink struct {
	ast.BaseInline
	Source string
	Exists bool
}

// Kind reports the kind of this node.
func (n *wimglink) Kind() ast.NodeKind {
	return KindWikiImgLink
}

// Dump dumps the Node to stdout.
func (n *wimglink) Dump(src []byte, level int) {
	ast.DumpHelper(n, src, level, map[string]string{
		"Source": n.Source,
	}, nil)
}

type wlParser struct{}
type wlRenderer struct{}
type wlExtension struct{}

func (r *wlRenderer) RegisterFuncs(register renderer.NodeRendererFuncRegisterer) {
	register.Register(KindWikiImgLink, r.render)
}

func NewRenderer() renderer.NodeRenderer {
	return &wlRenderer{}
}

// WikiImgLink is a goldmark extension configured with default options.
var WikiImgLink = newWithDefaultOptions()

// New returns a goldmark extension that can be configured with options.
func New() goldmark.Extender {
	ext := newWithDefaultOptions()

	return ext
}

func newWithDefaultOptions() *wlExtension {
	return &wlExtension{}
}

func (wp *wlParser) Trigger() []byte {
	return []byte("![[")
}

func (ext *wlExtension) Extend(m goldmark.Markdown) {
	// The link parser is at priority 200 in goldmark so we need to be
	// lower than that to ensure that the "[" trigger fires.
	m.Parser().AddOptions(
		parser.WithInlineParsers(
			util.Prioritized(&wlParser{}, 199),
		),
	)
	m.Renderer().AddOptions(renderer.WithNodeRenderers(
		util.Prioritized(NewRenderer(), 500),
	))
}

// Parse implements the parser.Parser interface for Wikilinks in markdown
func (p *wlParser) Parse(parent ast.Node, block text.Reader, pc parser.Context) ast.Node {

	l, _ := block.PeekLine()
	// parse gets triggered with `![[`
	// also it has to be > 5 chars
	line := string(l)
	if len(l) <= 5 || !strings.HasPrefix(line, _open) {
		return nil
	}

	closePos := strings.Index(line, _close)
	// ignore empty links
	if closePos <= 3 {
		return nil
	}
	raw := line[3:closePos]
	// TODO: check if this is an image link

	wl := &wimglink{}
	wl.Source = raw

	wl.Exists = true // TODO

	block.Advance((closePos + 3)) // 2 coz open delimitors = `[[`

	return wl
}

func (r *wlRenderer) render(w util.BufWriter, source []byte, node ast.Node, entering bool) (ast.WalkStatus, error) {
	if !entering {
		return ast.WalkContinue, nil
	}

	wl := node.(*wimglink)
	out := fmt.Sprintf(`<img src="%s" loading="lazy"/>`, wl.Source)

	w.Write([]byte(out))
	return ast.WalkContinue, nil
}
