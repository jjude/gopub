package highlighter

import (
	"fmt"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer"
	"github.com/yuin/goldmark/text"
	"github.com/yuin/goldmark/util"
)

// parses for ==highlightedsource== and adds a highlighter <mark>

const (
	_highlighter string = "=="
)

// KindHighlighter is the kind of the highlighter AST node.
var KindHighlighter = ast.NewNodeKind("Highlighter")

type highlighter struct {
	ast.BaseInline
	Source string
}

// Kind reports the kind of this node.
func (n *highlighter) Kind() ast.NodeKind {
	return KindHighlighter
}

// Dump dumps the Node to stdout.
func (n *highlighter) Dump(src []byte, level int) {
	ast.DumpHelper(n, src, level, map[string]string{
		"Source": n.Source,
	}, nil)
}

type wlParser struct{}
type wlRenderer struct{}
type wlExtension struct{}

func (r *wlRenderer) RegisterFuncs(register renderer.NodeRendererFuncRegisterer) {
	register.Register(KindHighlighter, r.render)
}

func NewRenderer() renderer.NodeRenderer {
	return &wlRenderer{}
}

// HighlighterLink is a goldmark extension configured with default options.
var HighlighterLink = newWithDefaultOptions()

// New returns a goldmark extension that can be configured with options.
func New() goldmark.Extender {
	ext := newWithDefaultOptions()

	return ext
}

func newWithDefaultOptions() *wlExtension {
	return &wlExtension{}
}

func (wp *wlParser) Trigger() []byte {
	return []byte("==")
}

func (ext *wlExtension) Extend(m goldmark.Markdown) {
	// The link parser is at priority 200 in goldmark so we need to be
	// lower than that to ensure that the "==" trigger fires.
	m.Parser().AddOptions(
		parser.WithInlineParsers(
			util.Prioritized(&wlParser{}, 199),
		),
	)
	m.Renderer().AddOptions(renderer.WithNodeRenderers(
		util.Prioritized(NewRenderer(), 500),
	))
}

// Parse implements the parser.Parser interface for Wikilinks in markdown
func (p *wlParser) Parse(parent ast.Node, block text.Reader, pc parser.Context) ast.Node {

	l, _ := block.PeekLine()
	// parse gets triggered with `==`
	// also it has to be > 4 chars (with open and end of ==)
	line := string(l)
	if len(l) <= 4 || !strings.HasPrefix(line, _highlighter) {
		return nil
	}

	// because open & close are same; parser could be triggered by close too
	// so we pick from line[2:]
	closePos := strings.Index(line[2:], _highlighter)
	// ignore empty highlights; there could be one letter highlighted too
	// or there could be none => closePos = -1
	if closePos <= 0 {
		return nil
	}
	raw := line[2 : 2+closePos]

	wl := &highlighter{}
	wl.Source = raw

	block.Advance((closePos + 4)) // 4 coz of 2 delimitors `==`

	return wl
}

func (r *wlRenderer) render(w util.BufWriter, source []byte, node ast.Node, entering bool) (ast.WalkStatus, error) {
	if !entering {
		return ast.WalkContinue, nil
	}

	wl := node.(*highlighter)
	out := fmt.Sprintf(`<mark>%s</mark>`, wl.Source)

	w.Write([]byte(out))
	return ast.WalkContinue, nil
}
