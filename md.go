package main

import (
	"bytes"
	hl "gopub/highlighter"
	wil "gopub/wikiimglink"
	wl "gopub/wikilink"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
)

func mdify(content []byte) string {
	md := goldmark.New(
		goldmark.WithExtensions(
			extension.GFM,
			extension.DefinitionList,
			extension.Footnote,
			extension.Typographer,
			wl.WikiLink,
			wil.WikiImgLink,
			hl.HighlighterLink,
		),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithXHTML(),
		),
	)
	var buf bytes.Buffer
	md.Convert(content, &buf)

	return buf.String()
}
