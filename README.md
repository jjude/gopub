# gopub

Tool to publish obsidian vault (a set of directory of .md files with associated images) as static pages, which can be hosted on Github, Gitlab, or Netlify.

## Features
- sitewide search
- read metadata for titles, if present
- support tags in the format of #tags
- backlinks
- convert .md files into htmls ✅
- convert filenames into slugs ✅
- support markdown links ✅
- support [[wikilinks]] ✅
- support images of format ![[image]] ✅
- simple design based on tachyons ✅
- indicate external links differently ✅
- open external links in different tab  ✅

## Bugs
- [[#section|section]] -> doesn't link to the section within the same doc

## Known Limitations
- all assets have to be in the same dir - not sub-directory

## Non-Goals
- will not support obsidian kind of graph

## Generating binary
`GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" .`

and then compress with `upx`

`upx --best --lzma gopub`

## Installation


## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status
Started development in Dec'22
