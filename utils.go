package main

import (
	"strings"

	"github.com/extemporalgenome/slug"
)

func slugifyDir(dir string) string {
	s := strings.SplitN(dir, "/", -1)
	ds := ""
	for i, v := range s {
		// if it is going to subdir then don't sluggify
		if v == ".." {
			ds = ds + v
		} else {
			ds = ds + slug.Slug(v)
		}
		if i != len(s)-1 {
			ds = ds + "/"
		}
	}
	return ds
}
